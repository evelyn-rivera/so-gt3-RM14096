#include "libstop.h"
#include <stdio.h>

int main(){
int num, res;

    printf("Ingrese el numero para elevarlo al cubo:\t");
    scanf("%d",&num);

    if (num<0){
         printf("Error ingrese un valor correcto");
    }else{
        
        res=cubo(num);
      printf("El resultado de elevar %d al cubo es: %d\t\n",num,res);
 }

    return 0;
}


long cubo (int num){
    if(num==0 || num==1){
        return 1;
    }else if(num>1){
        return (num*num*num);
    }
}