#include "libstop.h"
#include <stdio.h>

int main(){
int numero, rest;

    printf("Ingrese el numero que desea elevar al cubo:\t");
    scanf("%d",&numero);

    if (numero<0){
         printf("Error ingrese un valor correcto");
    }else{
        
        rest=cubo(numero);
      printf("El resultado de elevar %d al cubo es: %d\t\n",numero,rest);
 }

    return 0;
}


long cubo (int numero){
    if(numero==0 || numero==1){
        return 1;
    }else if(numero>1){
        return (numero*numero*numero);
    }
}