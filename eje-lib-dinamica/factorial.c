#include "libstop.h"
#include <stdio.h>

int main() {
    int num;
    int fact;
    
    printf("Ingrese el dato:\t \n");
    scanf("%d",&num);
    
    if (num<0){
        printf("Error ingrese el dato correcto");
    
    }else{
        fact=factorial(num);
        
        printf("El factorial del numero es %d!=%d\n",num,fact);
    }
return 0;
}

long factorial (int num){
    if(num==0 || num==1){
        return 1;
    }else if(num>2) {
        int factorial=1;
        while (num>1)
        {
         factorial*=num; 
         num --;  
        }
        return factorial;

        }   
    }